package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Genre;
import com.example.Movie.models.dto.GenreDTO;
import com.example.Movie.repositories.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {
	
	@Autowired
	GenreRepository genreRepository;
	
	// Get All Actor mapper
	@GetMapping("/genres/readMapper")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Genre dengan menggunakan method findAll() dari repository
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<GenreDTO> listGenreDTO = new ArrayList<GenreDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Genre genre : listGenreEntity) {

			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
			
			//memasukan object paper DTO ke arraylist
			listGenreDTO.add(genreDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Genre Data Success");
		result.put("Data", listGenreDTO);
						
		return result;
	}
	
	// Create a new Genre Mapper
	@PostMapping("/genres/createMapper")
	public HashMap<String, Object> createGenreMapper(@Valid @RequestBody GenreDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Genre genre = modelMapper.map(body, Genre.class);
		
		//ini proses save data ke database
		genreRepository.save(genre);	
		body.setId(genre.getId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Genre Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Update a Genre Mapper
	@PutMapping("/genres/updateMapper/{id}")
	public HashMap<String, Object> updateGenreMapper(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody GenreDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Genre genreEntity = genreRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Genre", "id", id));
		
		ModelMapper modelMapper = new ModelMapper();
		genreEntity = modelMapper.map(body, Genre.class);
		genreEntity.setId(id);
		
		//ini proses save data ke database
		genreRepository.save(genreEntity);
		body = modelMapper.map(genreEntity, GenreDTO.class);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Genre Success");
		result.put("Data", body);
		
		return result;
	}
	
	@DeleteMapping("/genres/deleteMapper/{id}")
	public HashMap<String, Object> deleteGenreMapper(@PathVariable(value = "id") Long id, GenreDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Genre genreEntity = genreRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Genre", "id", id));
		
		ModelMapper modelMapper = new ModelMapper();
		genreEntity = modelMapper.map(body, Genre.class);
		genreEntity.setId(id);
	
		genreRepository.delete(genreEntity);	
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete genre Success");
		
		return result;
	}

}

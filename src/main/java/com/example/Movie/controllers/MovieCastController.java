package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.models.MovieCast;
import com.example.Movie.models.dto.MovieCastDTO;
import com.example.Movie.repositories.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {
	
	@Autowired
	MovieCastRepository movieCastRepository;
	
	// Get All MovieCast mapper
	@GetMapping("/movieCast/readMapper")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari MovieCast dengan menggunakan method findAll() dari repository
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<MovieCastDTO> listMovieCastDTO = new ArrayList<MovieCastDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(MovieCast movieCast : listMovieCastEntity) {

			MovieCastDTO movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			
			//memasukan object paper DTO ke arraylist
			listMovieCastDTO.add(movieCastDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All MovieCast Data Success");
		result.put("Data", listMovieCastDTO);
						
		return result;
	}
	
	// Create a new MovieCast DTO Mapper
	@PostMapping("/movieCast/createMapper")
	public HashMap<String, Object> createMovieCastMapper(@Valid @RequestBody MovieCastDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		MovieCast movieCastEntity = modelMapper.map(body, MovieCast.class);
		
		//ini proses save data ke database
		movieCastRepository.save(movieCastEntity);
		body.setId(movieCastEntity.getId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New MovieCast Success");
		result.put("Data", body);
		
		return result;
		
	}
	
	// Update a MovieCast Mapper
	@PutMapping("/movieCast/updateMapper/{actId}/{movId}")
	public HashMap<String, Object> updateMovieCastMapper(@PathVariable(value = "actId") Long actId, 
			@PathVariable(value = "movId") Long movId, @Valid @RequestBody MovieCastDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		boolean isValid = false;
		
		ArrayList<MovieCast> listMovieCastEntities = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		ModelMapper modelMapper = new ModelMapper();
	
		
		for (MovieCast movieCast : listMovieCastEntities) {
			if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
			
				movieCast.setRole(body.getRole());
				
				movieCastRepository.save(movieCast);
				body = modelMapper.map(movieCast, MovieCastDTO.class);
				isValid = true;
			}
	
		}
		if (isValid) {	
			//Maping result untuk response API
			result.put("Status", 200);
			result.put("Message", "Update MovieCast Success");
			result.put("Data", body);
		}
		else {
			result.put("Message", "Data Not Found");
		}
		return result;
		
	}
	
	// Delete a MovieCast Mapper
	@DeleteMapping("/movieCast/deleteMapper/{actId}/{movId}")
	public HashMap<String, Object> deleteMovieCastMapper(@PathVariable(value = "actId") Long actId, 
			@PathVariable(value = "movId") Long movId, MovieCastDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		boolean isValid = false;
		
		ArrayList<MovieCast> listMovieCastEntities = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		ModelMapper modelMapper = new ModelMapper();
	
		
		for (MovieCast movieCast : listMovieCastEntities) {
			if(movieCast.getId().getActId() == actId && movieCast.getId().getMovId() == movId) {
				
				movieCastRepository.delete(movieCast);
				body = modelMapper.map(movieCast, MovieCastDTO.class);
				isValid = true;
			}
			
			else {
				isValid = false;
			}
	
		}
		if (isValid == true) {	
			//Maping result untuk response API
			result.put("Status", 200);
			result.put("Message", "Delete MovieCast Success");
			result.put("Data", body);
		} else {
			result.put("Message", "Data Not Found");
		}
		return result;
		
	}

}

package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Director;
import com.example.Movie.models.dto.DirectorDTO;
import com.example.Movie.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	
	@Autowired
	DirectorRepository directorRepository;
	
	// Get All Director mapper
	@GetMapping("/directors/readMapper")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Director dengan menggunakan method findAll() dari repository
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<DirectorDTO> listDirectorDTO = new ArrayList<DirectorDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Director director : listDirectorEntity) {

			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
			
			//memasukan object paper DTO ke arraylist
			listDirectorDTO.add(directorDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Director Data Success");
		result.put("Data", listDirectorDTO);
						
		return result;
	}
	
	// Create a new Director Mapper
	@PostMapping("/directors/createMapper")
	public HashMap<String, Object> createDirectorDTO(@Valid @RequestBody DirectorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Director director = modelMapper.map(body, Director.class);
		
		//ini proses save data ke database
		directorRepository.save(director);	
		body.setId(director.getId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Director Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Update a Director Mapper
	@PutMapping("/directors/updateMapper/{id}")
	public HashMap<String, Object> updateDirectorMapper(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody DirectorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Director directorEntity = directorRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Director", "id", id));
		
		ModelMapper modelMapper = new ModelMapper();
		directorEntity = modelMapper.map(body, Director.class);
		directorEntity.setId(id);
		
		//ini proses save data ke database
		directorRepository.save(directorEntity);
		body = modelMapper.map(directorEntity, DirectorDTO.class);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Director Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Delete a Director Mapper
	@DeleteMapping("/directors/deleteMapper/{id}")
	public HashMap<String, Object> deleteDirectorMapper(@PathVariable(value = "id") Long id, DirectorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Director directorEntity = directorRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Director", "id",  id));
		
		ModelMapper modelMapper = new ModelMapper();
		directorEntity = modelMapper.map(body, Director.class);
		directorEntity.setId(id);
	
		directorRepository.delete(directorEntity);	
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Director Success");
		
		return result;
	}

}

package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.models.Actor;
import com.example.Movie.models.dto.ActorDTO;
import com.example.Movie.repositories.ActorRepository;
import com.example.Movie.exceptions.ResourceNotFoundException;

@RestController
@RequestMapping("/api")
public class ActorController {
	
	@Autowired
	ActorRepository actorRepository;
	
	// Get All Actor mapper
	@GetMapping("/actors/readMapper")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari actor dengan menggunakan method findAll() dari repository
		ArrayList<Actor> listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<ActorDTO> listActorDTO = new ArrayList<ActorDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Actor actor : listActorEntity) {

			ActorDTO actorDTO = modelMapper.map(actor,ActorDTO.class);
			
			//memasukan object paper DTO ke arraylist
			listActorDTO.add(actorDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All actor Data Success");
		result.put("Data", listActorDTO);
						
		return result;
	}
	
	// Create a new actor Mapper
	@PostMapping("/actors/createMapper")
	public HashMap<String, Object> createActorDTO(@Valid @RequestBody ActorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Actor actor = modelMapper.map(body,Actor.class);
		
		//ini proses save data ke database
		actorRepository.save(actor);	
		body.setId(actor.getId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Actor Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Update a Actor Mapper
	@PutMapping("/actors/updateMapper/{id}")
	public HashMap<String, Object> updateActorMapper(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody ActorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Actor actorEntity = actorRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
		
		ModelMapper modelMapper = new ModelMapper();
		actorEntity = modelMapper.map(body,Actor.class);
		actorEntity.setId(id);
		
		//ini proses save data ke database
		actorRepository.save(actorEntity);
		body = modelMapper.map(actorEntity, ActorDTO.class);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Actor Success");
		result.put("Data", body);
		
		return result;
	}
	
	@DeleteMapping("/actors/deleteMapper/{id}")
	public HashMap<String, Object> deleteActorMapper(@PathVariable(value = "id") Long id, ActorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Actor actorEntity = actorRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Actor", "id",  id));
		
		ModelMapper modelMapper = new ModelMapper();
		actorEntity = modelMapper.map(body,Actor.class);
		actorEntity.setId(id);
	
		actorRepository.delete(actorEntity);	
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Actor Success");
		
		return result;
	}

}

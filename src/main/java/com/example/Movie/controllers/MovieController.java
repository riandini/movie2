package com.example.Movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Movie.exceptions.ResourceNotFoundException;
import com.example.Movie.models.Movie;
import com.example.Movie.models.dto.MovieDTO;
import com.example.Movie.repositories.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {
	
	@Autowired
	MovieRepository movieRepository;
	
	// Get All Movie mapper
	@GetMapping("/movies/readMapper")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Movie dengan menggunakan method findAll() dari repository
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<MovieDTO> listMovieDTO = new ArrayList<MovieDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Movie movie : listMovieEntity) {

			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
			
			//memasukan object paper DTO ke arraylist
			listMovieDTO.add(movieDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Movie Data Success");
		result.put("Data", listMovieDTO);
						
		return result;
	}
	
	// Create a new Movie Mapper
	@PostMapping("/movies/createMapper")
	public HashMap<String, Object> createMovieDTO(@Valid @RequestBody MovieDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Movie movie = modelMapper.map(body, Movie.class);
		
		//ini proses save data ke database
		movieRepository.save(movie);	
		body.setId(movie.getId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Movie Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Update a Movie Mapper
	@PutMapping("/movies/updateMapper/{id}")
	public HashMap<String, Object> updateMovieMapper(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody MovieDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Movie movieEntity = movieRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Movie", "id", id));
		
		ModelMapper modelMapper = new ModelMapper();
		movieEntity = modelMapper.map(body, Movie.class);
		movieEntity.setId(id);
		
		//ini proses save data ke database
		movieRepository.save(movieEntity);
		body = modelMapper.map(movieEntity, MovieDTO.class);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Movie Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Delete a Movie Mapper
	@DeleteMapping("/movies/deleteMapper/{id}")
	public HashMap<String, Object> deleteMovieMapper(@PathVariable(value = "id") Long id, MovieDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Movie movieEntity = movieRepository.findById(id)
	            .orElseThrow(() -> new ResourceNotFoundException("Movie", "id", id));
		
		ModelMapper modelMapper = new ModelMapper();
		movieEntity = modelMapper.map(body, Movie.class);
		movieEntity.setId(id);
	
		movieRepository.delete(movieEntity);	
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Movie Success");
		
		return result;
	}

}
